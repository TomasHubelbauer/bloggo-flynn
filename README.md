# Flynn

Flynn is an open source platform as a service software.
By installing Flynn into multiple servers and connecting them as nodes, high availability apps can be deployed easily.
The deployment is similar to Dokku (or Heroku) with `git push flynn master`.
Deis ([GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-deis), [Bloggo post](http://hubelbauer.net/post/deis)) is an alternative to Flynn.

## Links

- [GitHub repository](https://github.com/flynn/flynn)
- [DOfficial docs](https://flynn.io/docs/installation)
- [*Upgrading from Dokku to Flynn*](https://flynn.io/blog/upgrading-from-dokku-to-flynn)

## Next steps

- [ ] Read up on [Installing a cluster](https://flynn.io/docs/installation/manual)
- [ ] Buy 3 Wedos VPSs and set up a cluster
- [ ] Compare with Deis and note down the differences
